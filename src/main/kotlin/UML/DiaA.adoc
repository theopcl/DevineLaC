[plantuml]
----
class Jeu {
  avecAide : boolean
  ----
  demarre()
  isMatch(c : Carte) : boolean
  statistiquesDeLaPartie() : string
}

class Paquet {
  nom : String
  /cardinal : Int
  ---

}

class Carte {
  nom
  couleur
  /points : Int
  /valeur : Int
  ---
}

Jeu"*" -> "\ncarteADeviner  1  " Carte : "\t\t\t"
Jeu"*" -> " 1  " Paquet : "\t\t\t\t\t\t"
Paquet "    *    " --> "    * cartes  " Carte
hide circle
----